import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SnapshotService {
  map = {};
  constructor() { }
  
  set(key,val){
    this.map[key]=val;
  }
  get(key){
    return this.map[key];
  }
}
