import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { CustomerVerificationComponent } from './customer-verification/customer-verification.component';
import {TestingStuffComponentComponent} from './testing-stuff-component/testing-stuff-component.component';

const routes: Routes = [
    { path: 'welcome', component: WelcomeComponent },
    { path: 'testing', component: TestingStuffComponentComponent },
    {path:'confirmation', component: CustomerVerificationComponent},
    { path: '', redirectTo: '/welcome', pathMatch: 'full' },
    { path: '*', redirectTo: '/welcome' }
  ];
  
@NgModule({
    imports: [
      RouterModule.forRoot(routes, { useHash: true })
    ],
    exports: [
      RouterModule
    ],
    providers: []
  })
  export class AppRoutingModule {}