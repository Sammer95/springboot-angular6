import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpErrorResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import {SnapshotService} from '../snapshot.service';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.css']
})
export class WelcomeComponent implements OnInit {
  personalInfo = {
    firstName : "",
    lastName : "",
    email: "",
    dob: null,
    sin:"",
    cell:""
  }
  validName=true;
  validLastName=true;
  validEmail=true;
  validDate=true;
  validSIN=true;
  validCell=true;
  validStreet=true;
  autocomplete : any;
  constructor(private http: HttpClient, private router: Router, private snapshotService: SnapshotService) { 
    this.personalInfo = this.snapshotService.get("profile") ? snapshotService.get("profile"):   this.personalInfo;
  }

  ngOnInit() {
    //@ts-ignore
     $('[data-toggle="popover"]').popover();
    //@ts-ignore
    var defaultBounds = new google.maps.LatLngBounds(
      //@ts-ignore
    new google.maps.LatLng(-33.8902, 151.1759),
    //@ts-ignore
    new google.maps.LatLng(-33.8474, 151.2631));
    this.autocomplete = document.getElementById('autocomplete');
    var options = {
      bounds: defaultBounds,
      types: ['establishment'] 
    };

    // this.search = new google.maps.places.SearchBox(input, options);
    //@ts-ignore
    this.autocomplete = new google.maps.places.Autocomplete(this.autocomplete,{types: ['geocode']});

    // When the user selects an address from the dropdown, populate the address
    // fields in the form.
    this.autocomplete.addListener('place_changed', this.fillInAddress);

  }
  fillInAddress = function(){
    document.getElementById("street_number")["value"]="";
    document.getElementById("route")["value"]="";
    document.getElementById("neighborhood")["value"]="";
    document.getElementById("locality")["value"]="";
    document.getElementById("administrative_area_level_1")["value"]="";
    document.getElementById("postal_code")["value"]="";
    document.getElementById("country")["value"]="";
    var place = this.getPlace();
    for (var i = 0; i < place.address_components.length; i++) {
      var addressType = place.address_components[i].types[0];
      if (document.getElementById(addressType)) {
        var val = place.address_components[i].long_name;
        document.getElementById(addressType)["value"] = val;
      }
    }
    
  }
  onNameChange = function(newValue) {
    const firstNameRegex = /^[a-z ,.'-]+$/i;
    if (firstNameRegex.test(newValue)) {
        this.validName = true;
    }else {
      this.validName = false;
    }
  }
  onLastNameChange = function(newValue){
    const lastNameRegex = /^[a-z ,.'-]+$/i;
    if (lastNameRegex.test(newValue)) {
        this.validLastName = true;
    }else {
      this.validLastName = false;
    }
  }
  onEmailChange = function(newValue){
    const emailRegex =/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (emailRegex.test(newValue)) {
        this.validEmail = true;
    }else {
      this.validEmail = false;
    }
  }
  onDateChange = function(newVal){
    this.personalInfo.dob = newVal;
    var dob = new Date(newVal);
    var currentDate = new Date();
    var ageReq = 568061000000; //> Over 18
    var age = currentDate.getTime() - dob.getTime();

    this.validDate =  age>ageReq;
  }
  onCellChange = function(newValue){
    newValue = newValue.replace(/[(a-zA-z),-]/g,'');
    const lastNameRegex = /^1?(\d{3}\d{3}\d{4})$/;
    if (lastNameRegex.test(newValue)) {
        this.validCell = true;
    }else {
      this.validCell = false;
    }
  }
  onSINChange = function(newVal){
    const sinRegex =/^(\d{3}-\d{3}-\d{3})$/;
    if (sinRegex.test(newVal)) {
        this.validSIN = true;
    }else {
      this.validSIN = false;
    }
  }
  showSin = function(){
    var sin = document.getElementById("sin");
    if (sin["type"] === "password") {
      sin["type"] = "text";
    } else {
      sin["type"] = "password";
    }
  }
  onStreetChange = function(newStr){
  }
  submitForm = function(){
    this.onNameChange(this.personalInfo.firstName);
    this.onLastNameChange(this.personalInfo.lastName);
    this.onEmailChange(this.personalInfo.email);
    this.validDate = this.personalInfo.dob !== null; 
    this.onCellChange(this.personalInfo.cell);
    this.onSINChange(this.personalInfo.sin);
    if(!this.invalidForm()){
      this.updateProfile();
    }
    this.personalInfo["street_number"] = document.getElementById("street_number")["value"];
    this.personalInfo["route"] = document.getElementById("route")["value"];
    this.personalInfo["neighborhood"] = document.getElementById("neighborhood")["value"];
    this.personalInfo["locality"] = document.getElementById("locality")["value"];
    this.personalInfo["administrative_area_level_1"] = document.getElementById("administrative_area_level_1")["value"];
    this.personalInfo["postal_code"] = document.getElementById("postal_code")["value"];
    this.personalInfo["country"] = document.getElementById("country")["value"];
  }
  invalidForm = function(){
    return !(this.validName&&this.validLastName&&this.validEmail&&this.validDate&&this.validCell&&this.validSIN);
  }
  updateProfile = function(){
    this.http.post("http://localhost:8080/add-customer",this.personalInfo).subscribe(data=>
      {
        this.personalInfo["customerId"] =  data["customerId"] ? data["customerId"] : "";
        this.snapshotService.set("profile",this.personalInfo);
        this.router.navigate(['/confirmation']);
      },
      (err: HttpErrorResponse)=>{

      }
    );
  }
}
