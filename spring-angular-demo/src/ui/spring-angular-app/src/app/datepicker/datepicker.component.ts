import { Component,EventEmitter, OnInit, Input,Output } from '@angular/core';
@Component({
  selector: 'date-picker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.css']
})
export class DatepickerComponent implements OnInit {
  startDate = new Date()
  constructor() { }
  @Input('model') datePicker: any;
  @Output() dateChanged = new EventEmitter()
  
  ngOnInit() {
  }
  changedDate = function(): Date{
    var selectedDate = new Date(this.datePicker);
    this.dateChanged.emit(selectedDate);
    return selectedDate;
  }
}
