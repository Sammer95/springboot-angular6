import { Component, OnInit, ElementRef,AfterContentInit, Input  } from '@angular/core';

@Component({
  selector: 'tool-tip',
  templateUrl: './tool-tip.component.html',
  styleUrls: ['./tool-tip.css']
})
export class ToolTipComponent implements OnInit {
  @Input('text') text: String

  constructor(el: ElementRef) {
   }
  ngOnInit() {
  }
}
