import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import {AppRoutingModule} from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { Http} from '@angular/http';
import { HeaderComponent } from './header/header.component';
import { FormsModule } from '@angular/forms';
import {sin} from './filters/sinFilter';
import {phone} from './filters/phoneFilter';
import { SinDirective } from './sin.directive';
import { PhoneDirective } from './phone.directive';
import { CustomerVerificationComponent } from './customer-verification/customer-verification.component';
import { TestingStuffComponentComponent } from './testing-stuff-component/testing-stuff-component.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { ToolTipComponent } from './tool-tip/tool-tip.component';
import { DatepickerComponent } from './datepicker/datepicker.component';

import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatNativeDateModule} from '@angular/material'
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material';

@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    HeaderComponent,
    sin,
    phone,
    SinDirective,
    PhoneDirective,
    CustomerVerificationComponent,
    TestingStuffComponentComponent,
    ToolTipComponent,
    DatepickerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatFormFieldModule,
    MatInputModule
  ],
  providers: [Http],
  bootstrap: [AppComponent]
})
export class AppModule { }
