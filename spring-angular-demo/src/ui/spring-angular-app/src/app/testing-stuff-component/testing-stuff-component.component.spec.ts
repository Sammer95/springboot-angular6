import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestingStuffComponentComponent } from './testing-stuff-component.component';

describe('TestingStuffComponentComponent', () => {
  let component: TestingStuffComponentComponent;
  let fixture: ComponentFixture<TestingStuffComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestingStuffComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingStuffComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
