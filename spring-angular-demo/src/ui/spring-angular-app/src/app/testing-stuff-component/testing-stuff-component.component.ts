import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-testing-stuff-component',
  templateUrl: './testing-stuff-component.component.html',
  styleUrls: ['./testing.css']
})
export class TestingStuffComponentComponent implements OnInit {

  constructor() { 
  }

  ngOnInit() {
   //@ts-ignore
    $('[data-toggle="popover"]').popover();
    
    //@ts-ignore
   /* can be used in any tag, and will only close when clicking the same tag or close button
      $("#firstNameTooltip").popover(
      {
        placement: 'right',
        html: true,
        content : '<button type="button" id="close" class="close text-primary" onclick="$(&quot;#firstNameTooltip&quot;).popover(&quot;hide&quot;);">&times;</button>'+
        '<br/><div class="text-secondary"><strong>Please enter your first name</strong></div>'
      }
    );
    
    */
  }

}
