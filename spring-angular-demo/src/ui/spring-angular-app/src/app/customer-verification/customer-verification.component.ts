import { Component, OnInit } from '@angular/core';

import {SnapshotService} from '../snapshot.service';

@Component({
  selector: 'app-customer-verification',
  templateUrl: './customer-verification.component.html',
  styleUrls: ['./customer-verification.component.css']
})
export class CustomerVerificationComponent implements OnInit {
  personalInfo = {};
  constructor(private snapshotService:SnapshotService) {
    this.personalInfo = snapshotService.get("profile");
   }

  ngOnInit() {
  }

}
